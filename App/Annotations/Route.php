<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 20:57:31
 */

namespace App\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Route
 *
 * @package App\Annotations
 * @Annotation
 */
class Route
{
    /**
     * @var array
     */
    public $methods = ['GET'];
}