<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 20:57:31
 */

namespace App\Annotations;

/**
 * Class TokenRequired
 *
 * @package App\Annotations
 * @Annotation
 */
class TokenRequired
{
    /**
     * @var string
     */
    public $type = 'header';
}