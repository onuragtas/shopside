<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 17:8:9
 */

namespace App\Controllers;


/**
 * Class BaseController
 *
 * @package App\Controllers
 */
abstract class BaseController
{
    public $user;
    /**
     * @return array
     */
    public function getRaw(){
        return json_decode(file_get_contents('php://input'), true);
    }

}