<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 16:42:26
 */

namespace App\Controllers;

use App\Annotations\TokenRequired;
use App\Models\Product as ProductModel;
use App\Utility\ResponseHelper;
use App\Annotations\Route;

/**
 * Class User
 *
 * @package App\Controllers
 */
class Product extends BaseController
{

    /**
     * @Route(methods={"GET", "POST"})
     * @TokenRequired
     */
    public function list()
    {
        $productModel = new ProductModel();
        list($allProducts, $count) = $productModel->find($_GET['order'], $_GET['start'], $_GET['length'], $_GET['search']['value']);
        $response = [
            'draw' => intval($_GET['draw']),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => $allProducts
        ];
        ResponseHelper::jsonResponse($response);
    }

    /**
     * @TokenRequired
     * @Route(methods={"POST"})
     */
    public function add()
    {
        $data = $this->getRaw(); // TODO Must be data validate
        $model = new ProductModel();
        $result = $model->addProduct($data);
        if ($result){
            ResponseHelper::jsonResponse([]);
        }
    }
}