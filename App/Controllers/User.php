<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 16:42:26
 */

namespace App\Controllers;

use App\Annotations\Route;
use App\Models\User as UserModel;
use App\Utility\ResponseHelper;
use Firebase\JWT\JWT;

/**
 * Class User
 *
 * @package App\Controllers
 */
class User extends BaseController
{

    /**
     * @Route(methods={"POST"})
     */
    public function Login()
    {
        $data = $this->getRaw();
        $model = new UserModel();
        $user = $model->findByUsernameAndPassword($data['username'], $data['password']);
        if (empty($user)){
            ResponseHelper::jsonResponse(['message'=>'User not found']);
        }

        $payload = array(
            "iss" => "http://shopside.io",
            "aud" => "http://shopside.io",
            "iat" => time(),
            "exp" => time()+3600,
            'payload' => [
                'user_id' => $user[0]['id']
            ]
        );

        $jwt = JWT::encode($payload, JWT_KEY);
        ResponseHelper::jsonResponse(['token'=>$jwt]);
    }

    public function decode(string $jwt){

    }

}