<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 17:10:18
 */

namespace App\Core;

use PDO;

/**
 * Class Database
 *
 * @package App\Core
 */
class Database
{
    /**
     * @var PDO
     */
    private $database;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        $dsn = 'mysql:dbname='.DB_NAME.';host='.DB_HOST;

        $this->database = new PDO($dsn, DB_USERNAME, DB_PASSWORD,[
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
    }

    /**
     * @param string $query
     * @param array  $bindParams
     *
     * @param int    $mode
     *
     * @return array
     */
    protected function executeQuery(string $query, array $bindParams = [], int $mode = PDO::FETCH_ASSOC): array
    {
        $stmt = $this->database->prepare($query);
        $stmt->execute($bindParams);
        return $stmt->fetchAll($mode);
    }

    /**
     * @param string $query
     * @param array  $bindParams
     *
     * @return bool
     */
    protected function insertQuery(string $query, array $bindParams = []): bool
    {
        $stmt = $this->database->prepare($query);
        return $stmt->execute($bindParams);
    }

}