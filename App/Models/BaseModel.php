<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 17:15:38
 */

namespace App\Models;


use App\Core\Database;

/**
 * Class BaseModel
 *
 * @package App\Models
 */
abstract class BaseModel extends Database
{

}