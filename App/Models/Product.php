<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 17:7:44
 */

namespace App\Models;

use PDO;

/**
 * Class User
 *
 * @package App\Models
 */
class Product extends BaseModel
{

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->executeQuery('SELECT * FROM products');
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public function addProduct(array $data)
    {
        $bind = [
            'title' => $data['title'],
            'description' => $data['description'],
            'price' => $data['price'],
            'discount' => $data['discount'],
            'active' => $data['active'],
        ];

        return $this->insertQuery('INSERT INTO products 
        (title, description, price, discount, active)
        VALUES
        (:title, :description, :price, :discount, :active)', $bind);
    }

    /**
     * @param $order
     * @param $start
     * @param $length
     * @param $value
     *
     * @return array
     */
    public function find($order, int $start, int $length, $value)
    {
        $bind = [];

        $data = $this->executeQuery(sprintf('SELECT id, title, description, price, discount, active FROM products LIMIT %d, %d',
            $start, $length), $bind, PDO::FETCH_NUM);
        $count = $this->executeQuery('SELECT COUNT(0) as count FROM products');
        return [$data, intval($count[0]['count'])];
    }
}