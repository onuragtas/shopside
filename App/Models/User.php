<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 17:7:44
 */

namespace App\Models;

/**
 * Class User
 *
 * @package App\Models
 */
class User extends BaseModel
{

    /**
     * @param $username
     * @param $password
     *
     * @return array
     */
    public function findByUsernameAndPassword($username, $password): array
    {
        $password = md5($password);
        $bind = [
            ':username' => $username,
            ':password' => $password
        ];
        return $this->executeQuery('SELECT * FROM users WHERE username = :username AND password = :password', $bind);
    }
}