<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 20:59:27
 */

namespace App\Utility;


use Doctrine\Common\Annotations\AnnotationReader;
use ReflectionClass;
use ReflectionException;

class AnnotationReaderHelper
{

    private static $instance;
    /**
     * @var AnnotationReader
     */
    private $annotationReader;

    public static function getInstance(){
        if (self::$instance == null){
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
        $this->annotationReader = new AnnotationReader();
    }

    /**
     * @param $class
     *
     * @return array
     * @throws ReflectionException
     */
    public function readClass($class){
        $reflectionClass = new ReflectionClass($class);
        return $this->annotationReader->getClassAnnotations($reflectionClass);
    }

    /**
     * @param $class
     * @param $method
     *
     * @return array
     * @throws ReflectionException
     */
    public function readMethod($class, $method){
        $reflectionClass = new ReflectionClass($class);
        return $this->annotationReader->getMethodAnnotations($reflectionClass->getMethod($method));
    }

    /**
     * @param $class
     * @param $method
     *
     * @return array
     * @throws ReflectionException
     */
    public function getParameters($class, $method)
    {
        $reflectionClass = new ReflectionClass($class);
        $reflectionMethod = $reflectionClass->getMethod($method);
        $comment = $reflectionMethod->getDocComment();
        $parameters = [];

        $lines = explode("\n", $comment);
        foreach ($lines as $line) {
            if (strpos($line, '@param') !== false) {
                $re = '/\* @param (.*?) (.*?) (.*?) /mU';
                preg_match_all($re, $line.' ', $matches, PREG_SET_ORDER, 0);
                foreach ($matches as $match) {
                    $parameters[] = ['type' => trim($match[1]), 'variable' => trim($match[2]), 'required' => trim($match[3])];
                }
            }
        }

        return $parameters;
    }

}