<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 21:7:8
 */

namespace App\Utility;


class ResponseHelper
{

    /**
     * @param      $data
     * @param int  $code
     * @param bool $status
     */
    public static function jsonResponse($data = [], int $code = 200, bool $status = true)
    {
        header('Content-type: application/json');
        http_response_code($code);

        $result = [
            'status' => $status,
        ];
        if (!empty($data)) {
            $result['data'] = $data;
        }

        echo json_encode($result);
        die;
    }

}