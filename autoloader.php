<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 16:8:16
 */

/**
 * @param string $name
 */
function loader(string $name){
    $classPath = __DIR__.'/'.str_replace('\\', '/', $name).'.php';
    if (is_readable($classPath)) {
        require_once ($classPath);
    }
}

spl_autoload_register('loader');
include_once 'vendor/autoload.php';