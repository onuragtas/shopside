// Initiate datatables in roles, tables, users page
$('#dataTables-example').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "http://api.shopside.local.net/product/list",
        'headers': { 'Authorization': "Bearer "+localStorage.getItem('token') },
        "statusCode": {
            401: function (xhr, error, thrown) {
                debugger
                window.location.href = 'login.html';
                return false
            },
            403: function (xhr, error, thrown) {
                debugger
                window.location.href = 'login.html';
                return false
            }
        },
        dataSrc: function(json){
            json.draw = json.data.draw;
            json.recordsTotal = json.data.recordsTotal;
            json.recordsFiltered = json.data.recordsFiltered;

            return json.data.data;
        }
    }
}).on( 'error.dt', function ( e, settings, techNote, message ) {
    console.log( 'Error: Calendar DataTables: ' + message ); // for test purpose
    return true;
});