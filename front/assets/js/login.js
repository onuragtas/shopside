var login = {
    login: function (event) {
        event.preventDefault()

        let username = $("#inputUsername").val();
        let password = $("#inputPassword").val();

        requests.requests("POST", "user/login", {username, password}, function (response) {
            if (response.status){
                token = response.data.token;
                localStorage.setItem('token', token)
                window.location.href = 'index.html'
                alert("redirect")
            }
        })

        return false;
    }
}