$(document).ready(function () {
    main.checkLogin()
})


let main = {
    checkLogin: function () {
        let token = localStorage.getItem('token')
        if (token === '' || token == null) {
            window.location.href = 'login.html'
        }
    }
};