var product = {
    add: function (event) {
        event.preventDefault()

        let title = $("#title").val();
        let description = $("#description").val();
        let price = $("#price").val();
        let discount = $("input[name='discount']:checked").val();
        let active = $("input[name='active']:checked").val();

        requests.requests("POST", "product/add", {title, description, price, discount, active}, function (response) {
            if (response.status){
                window.location.href = 'index.html'
            }
        })

        return false;
    }
}