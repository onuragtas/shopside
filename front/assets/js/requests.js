var uri = 'http://api.shopside.local.net/'
var requests = {

    requests: function (method, endpoint, data, callback) {
        $.ajax({
            url: uri + endpoint,
            data: JSON.stringify(data),
            method: method,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem('token')
            },
            success: function (response) {
                callback(response);
            }
        })
    }
}
