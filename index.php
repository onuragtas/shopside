<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 17:6:25
 */

if (!function_exists('getallheaders'))
{
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}


include 'autoloader.php';
include 'config.php';

(new Router())->run();