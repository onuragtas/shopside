<?php
/*
 *  Created by PhpStorm.
 *  User: onuragtas
 *  Date: 14.12.2020
 *  Time: 16:8:2
 */

use App\Annotations\Route;
use App\Annotations\TokenRequired;
use App\Utility\AnnotationReaderHelper;
use App\Utility\ResponseHelper;
use Firebase\JWT\JWT;

/**
 * Class Router
 */
class Router
{

    /**
     * @var
     */
    public $user;
    /**
     * @var mixed
     */
    private $uri;
    /**
     * @var mixed|string
     */
    private $class;
    /**
     * @var mixed|string
     */
    private $method;
    /**
     * @var array
     */
    private $parameters;

    /**
     * Router constructor.
     *
     * @throws ReflectionException
     */
    public function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->parseUrl();
        $this->validateMethod();
    }

    /**
     *
     */
    private function parseUrl()
    {
        list($this->uri) = explode('?', $this->uri);
        $exploded = explode('/', substr($this->uri, 1, strlen($this->uri)));

        list($this->class, $this->method) = array_slice($exploded, 0, 2);
        $this->parameters = array_slice($exploded, 2, count($exploded) - 2);
        $this->class = ucfirst($this->class);
        $this->method = ucfirst($this->method);
        $className = 'App\\Controllers\\' . $this->class;
        $this->class = new $className;
    }

    /**
     * @throws ReflectionException
     */
    private function validateMethod()
    {
        $allowedMethods = null;
        $annotations = AnnotationReaderHelper::getInstance()->readMethod($this->class, $this->method);
        foreach ($annotations as $annotation) {
            if ($annotation instanceof Route) {
                $allowedMethods = $annotation->methods;

                if ((is_array($allowedMethods) && in_array($_SERVER['REQUEST_METHOD'],
                            $allowedMethods)) || $_SERVER['REQUEST_METHOD'] == $allowedMethods) {
                } else {
                    ResponseHelper::jsonResponse([
                        'message' => $_SERVER['REQUEST_METHOD'] . ' method not allowed',
                        'methods' => $annotation->methods
                    ]);
                }
            } else {
                if ($annotation instanceof TokenRequired) {
                    if ($annotation->type == 'header') {
                        $headers = getallheaders();
                        if (isset($headers[HEADER_TOKEN_KEY]) && !empty($headers[HEADER_TOKEN_KEY])) {
                            if (!$this->user = $this->checkToken($headers[HEADER_TOKEN_KEY])) {
                                $this->tokenException();
                            }
                        } else {
                            $this->requireToken();
                        }
                    }
                }
            }
        }

    }

    /**
     * @param string $token
     *
     * @return mixed
     */
    private function checkToken(string $token)
    {
        try {
            $token = str_replace('Bearer ', '', $token);
            $decoded = JWT::decode($token, JWT_KEY, array('HS256'));
            return $decoded->payload;
        }catch (Exception $e){
            return null;
        }
    }

    /**
     *
     */
    private function tokenException()
    {
        ResponseHelper::jsonResponse(['message' => 'Token not defined'], 403, false);
    }

    /**
     *
     */
    private function requireToken()
    {
        ResponseHelper::jsonResponse(['message' => 'Token required'], 401, false);
    }

    /**
     *
     */
    public function run()
    {
        $this->class->user = $this->user;

        call_user_func_array([($this->class), $this->method], $this->parameters);
    }
}