create table products
(
    id          int auto_increment
        primary key,
    title       varchar(255)         null,
    description varchar(255)         null,
    price       float(10, 2)         null,
    discount    tinyint(1) default 0 null,
    active      tinyint(1)           null
);

create table users
(
    id       int auto_increment,
    name     varchar(255) null,
    lastname varchar(25)  null,
    username varchar(255) null,
    email    varchar(255) null,
    password varchar(255) null,
    constraint users_id_uindex
        unique (id)
);

alter table users
    add primary key (id);

INSERT INTO users (name, lastname, username, email, password) VALUES ('Onur', 'Ağtaş', 'onuragtas', 'agtasonur@gmail.com', 'a34075cb1561aa96ac552840fb071ead');